package com.example.shoppingcart;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter{

	ArrayList<String> item, price;
	ArrayList<Integer> image;
	public static ArrayList<CheckBox> statusss;
	//ArrayList<String> checkedprice,checkeditem;
 	Context cxt;
	LayoutInflater inflater;
	int index=0;
	boolean chbx_sts;
	String obj;
	public static boolean checkingstatus[];
	int ar[];

	
	public ListAdapter(Context cxt, ArrayList<String> item, ArrayList<String> price,ArrayList<Integer> image, ArrayList<CheckBox> status){
		this.cxt=cxt;
		this.image = image;
		this.item=item;
		this.price=price;
		ListAdapter.statusss=status;
		
				
	}
	
	
	class ViewHolder{
		ImageView img;
		TextView item_name,item_price;
		CheckBox selectedornot; 
		
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return item.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;
				
		if(convertView==null){
		inflater=(LayoutInflater) cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.list_row, parent, false);
		
		holder = new ViewHolder();
		holder.item_name = (TextView)convertView.findViewById(R.id.textView1);
		//holder.txt_mail = (TextView)convertView.findViewById(R.id.person_age);
		holder.item_price= (TextView)convertView.findViewById(R.id.textView2);
		holder.img = (ImageView)convertView.findViewById(R.id.imageView1);
		holder.selectedornot = (CheckBox)convertView.findViewById(R.id.checkBox1);
		
		convertView.setTag( holder );
	}else {
        holder=(ViewHolder)convertView.getTag();
	}
		
	holder.item_name.setText(item.get(position));
	holder.item_price.setText(price.get(position));
	holder.img.setImageResource(image.get(position));
	holder.selectedornot.setChecked(false);
	holder.selectedornot.setButtonDrawable(R.drawable.exclude);
	
	holder.selectedornot.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			
		
			//ListAdapter.statusss.get(position).setChecked(isChecked);
			holder.selectedornot.setChecked(isChecked);
			
			/*checkingstatus[position]=isChecked;
			Log.e("Check Status", ""+checkingstatus.toString());*/
	
			if(holder.selectedornot.isChecked()==true){
				holder.selectedornot.setButtonDrawable(R.drawable.apply);	
				MainActivity.checkeditem.add(item.get(position));
				MainActivity.checkedprice.add(MainActivity.price_number.get(position));
				
			}
			if(holder.selectedornot.isChecked()==false){
				holder.selectedornot.setButtonDrawable(R.drawable.exclude);
				MainActivity.checkeditem.remove(item.get(position));
				MainActivity.checkedprice.remove(MainActivity.price_number.get(position));
				
			}
			
		}
	});	
	
		
		return convertView;
	}

}
