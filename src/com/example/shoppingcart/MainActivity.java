package com.example.shoppingcart;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;

public class MainActivity extends Activity {

	ListView shop_lst;
	ListAdapter ad;
	Button cart;
	ArrayList<String> item, price,cart_item,cart_price;
	ArrayList<Integer> image;
	ArrayList<CheckBox> status;
	public static ArrayList<String> checkeditem;
	public static ArrayList<Integer> checkedprice,price_number;
	
	String itm[]={"Shoes","Shirts","Pants","Coolers","Watch"};
	String prce[]={"Price : $25","Price : $40","Price : $65","Price : $17","Price : $32"};
	int price_num[]={25,40,65,17,32};
	int img[]={R.drawable.shoes,R.drawable.shirt,R.drawable.pants,R.drawable.glass,R.drawable.watch};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		checkeditem = new ArrayList<String>();
		checkedprice = new ArrayList<Integer>();
		price_number = new ArrayList<Integer>();
		
		cart = (Button)findViewById(R.id.bt_add_items_to_cart);
		
		item = new ArrayList<String>();
		price = new ArrayList<String>();
		image = new ArrayList<Integer>();
		status = new ArrayList<CheckBox>();
		
		for(int i=0;i<5;i++){
			item.add(itm[i]);
			price.add(prce[i]);
			image.add(img[i]);
			price_number.add(price_num[i]);		
		}
		
		
		shop_lst = (ListView)findViewById(R.id.shopping_list);
		
		ad = new ListAdapter(MainActivity.this, item, price,image, status);
		
		shop_lst.setAdapter(ad);
		
		cart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent i = new Intent(MainActivity.this, SecondActivity.class);
				startActivity(i);
				
				Log.e("Checking status of positions", ""+MainActivity.checkeditem.toString());
			//Toast.makeText(getApplicationContext(), "Items : "+MainActivity.checkeditem.toString()+"added to cart successfully", Toast.LENGTH_SHORT).show();	
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
