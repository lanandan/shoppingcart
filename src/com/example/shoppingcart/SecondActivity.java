package com.example.shoppingcart;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

public class SecondActivity extends Activity{

	ListView lst_items;
	TextView total;
	Button pay;
	ArrayList<String> st,st1;
	int a=0,b,c;
	ArrayAdapter ad;
	
	 //set the environment for production/sandbox/no network
		private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;	
		//Now set client id from the Paypal developer account-
		private static final String CONFIG_CLIENT_ID = "AavtrxBbrLt2rpvp3SrOIsRJHNbNvJTMTsL7K7GE0H7tmDHA9NdeMJEhTCOr";

		private static final int REQUEST_CODE_PAYMENT = 1;	
		private static PayPalConfiguration config = new PayPalConfiguration()
	    .environment(CONFIG_ENVIRONMENT)
	    .clientId(CONFIG_CLIENT_ID);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_two);
		
		st = new ArrayList<String>();
		st1 = new ArrayList<String>();
		lst_items = (ListView)findViewById(R.id.lst_view1);
		total = (TextView)findViewById(R.id.textView2);
		pay = (Button)findViewById(R.id.button1);
		
		for(int i=0;i<MainActivity.checkeditem.size();i++){
			st.add(MainActivity.checkeditem.get(i)+"       "+"$"+MainActivity.checkedprice.get(i));
			b=MainActivity.checkedprice.get(i);
			
			c=a+b;
			a=c;
			
		}
		
		if(MainActivity.checkeditem.size()==0){
	
			st1.add("No items in Cart");
			
			ad =  new ArrayAdapter<String>(this,
	                android.R.layout.simple_list_item_1, android.R.id.text1,st1 );
				
				lst_items.setAdapter(ad);
			
				total.setText("Go to shopping list to add items to your cart");
		
			
		}else{
			
			ad =  new ArrayAdapter<String>(this,
	                android.R.layout.simple_list_item_1, android.R.id.text1, st);
				
				lst_items.setAdapter(ad);
			
				total.setText("Total = $"+c);
			
		}
		
		Intent intent = new Intent(this, PayPalService.class);
	    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
	    startService(intent);
		
		pay.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				//Now you are ready to make a payment just on button press call the Payment Activity-		
				PayPalPayment thingToBuy= new PayPalPayment(new BigDecimal("1.75"),"USD", "Pair of shows",PayPalPayment.PAYMENT_INTENT_SALE);
			    Intent intent = new Intent(SecondActivity.this, PaymentActivity.class);
			    intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
			    startActivityForResult(intent, REQUEST_CODE_PAYMENT);    
				}
				// 8)And finally from the onActivityResult get the payment response-
				


			
		});
		
		
	}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	if (requestCode ==  REQUEST_CODE_PAYMENT) {
		if (resultCode == Activity.RESULT_OK) {
			PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
			if (confirm != null)
			{
			try
			{
		    System.out.println("Responseeee"+confirm);
			Log.i("paymentExample", confirm.toJSONObject().toString());
            JSONObject jsonObj=new JSONObject(confirm.toJSONObject().toString());
            String paymentId=jsonObj.getJSONObject("response").getString("id");
            System.out.println("payment id:-=="+paymentId);
			Log.i("Payment ID",paymentId.toString());
			}
			catch (JSONException e) 
			{
			Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
			}
			}
			}
			else if (resultCode == Activity.RESULT_CANCELED) 
			{
			 Log.i("paymentExample", "The user canceled.");
			}
			else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
			{
			Log.i("paymentExample", "An invalid Payment was submitted. Please see the docs.");
			}
			}
			}

	
	
}
